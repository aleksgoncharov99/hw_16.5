#include <iostream>

int main()
{
    const int N = 5;
    int array[N][N] = {0};

    // 1 & 2 task
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j];
            
        }
        std::cout << "\n";
    }
    
    std::cout << "\n" << "\n";

    // 3 task
    int index = 18 % N; // string index
    int sum = 0; // string summary
    for (int j = 0; j < N; j++)
    {
        sum = sum + array[index][j];
    }
    std::cout << sum;
   
}

